import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetAddress;

import javax.jms.*;

import org.apache.activemq.ActiveMQConnectionFactory;

public class ConsoleChat extends ChatBase implements MessageListener {

   boolean connected = false;
   BufferedReader commandLine;
   String inputText = "";

   /**
    * Chat constructor. Initializes the chat user name, topic, hostname.
    */
   public ConsoleChat() {
      super();
      commandLine = new java.io.BufferedReader(new InputStreamReader(System.in));
   }
   
   /*
    * To be overridden by derived classes to output senders message.
    */
   @Override
   protected void outputMessage (String sender, int type, String msgText) {
      System.out.println(sender + ": " + msgText + "\n");
   }
   
   @Override
   protected void initializeActiveMq(String args[]) {
      super.initializeActiveMq(args);
      
      String msgText = "";
      try {
         msgText = commandLine.readLine();
      } catch (IOException e) {
         e.printStackTrace();
      }
      
      //loop through till we read an exit.
      while(msgText.toLowerCase() != "exit" ){
         //update inputText as it would be used to sendNormalMessage..
         inputText = msgText;
         
         //Now send the normal message.
         sendNormalMessage();
         
         //read the next input line.
         try {
            msgText = commandLine.readLine();
         } catch (IOException e) {
            e.printStackTrace();
         }
      }
      
      //out of the loop close and shutdown.
      try {
         commandLine.close();
      } catch (IOException e) {
         e.printStackTrace();
      }
      commandLine = null;
      exit();
      
   }
   
   /**
    * Reads input from the console, and creates a message from it
    * If input is exit, the console is closed and exit is called
    */
   @Override
   protected String fetchInputMessage() throws Exception
   {
      return inputText;
   }

   /**
    * @param args
    *           Arguments passed via the command line. These are used to create
    *           the ConnectionFactory.
    */
   public static void main(String[] args) {
      ConsoleChat sc = new ConsoleChat();

      sc.initializeActiveMq(args);
   }

}
